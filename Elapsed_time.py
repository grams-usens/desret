# Print Elapsed time in standard format
def Elapsed_time(lap):
  hours = divmod(lap,3599)
  minutes = divmod(lap,59)  
  print('Elapsed iteration batch in: (%.0fh:%.0fmn:%.2fsec)' %(hours[0],minutes[0],minutes[1])) 
  BOT_MSG_UPDATE = 'Elapsed iteration batch time is in: (' + str(round(hours[0],0)) + 'h:' + str(round(minutes[0],0)) + 'mn:' + str(round(minutes[1],2)) + 'sec)'

  return BOT_MSG_UPDATE



print('Def Elapsed_time() successfully imported via Gtilab WEB IDE')
